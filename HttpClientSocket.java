import java.net.Socket;
import java.net.InetAddress;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * @Package: HttpClientSocket
 * @Author: Titouan 'Lux' Allain
 * @Version: 1.1
 */

public class ClientSocket
{
    public static void DownloadFile(BufferedReader inputStream, String page)
    {
        try
        {
            boolean created = false;
            File createdFile;
            if (page.equals("") || page.equals("/"))
            {
                page = "index.html";
            }
            String[] fileName = page.split("\\.");
            int fileOccurence = 0;
            do
            {
                // Setting name of the downloaded file
                if (fileOccurence == 0)
                {
                    createdFile = new File(fileName[0]+"."+fileName[1]);
                }
                else
                {
                    createdFile = new File(fileName[0] + "(" + Integer.toString(fileOccurence) + ")" + "." + fileName[1]);
                }
                // Page name doesn't localy exists statement
                if (createdFile.createNewFile())
                {
                    created = true;
                }
                // Page name localy exists statement
                else
                {
                    System.out.println(createdFile.getName() + " file already exists.");
                    fileOccurence++;
                }
            } while (!created);

            // Writing in the created file
            FileWriter dowloadedFile = new FileWriter(createdFile.getName());
            String httpResponse;
            // Reading headers
            while ( !inputStream.readLine().equals(""));
            // Writing content
            while ( (httpResponse = inputStream.readLine()) != null )
            {
                dowloadedFile.write(httpResponse);
                dowloadedFile.write("\n");
            }
            dowloadedFile.close();
            System.out.println(createdFile.getName() + " is downloaded.");
        }
        catch (Exception downloadException)
        {
            downloadException.printStackTrace();
        }
    }

    public static void SendHttpRequest(BufferedWriter httpRequest,Socket clientSocket, String host, String page, String httpVersion)
    {
        String CRLF = "\r\n";
        try
        {
            // Creating HTTP request
            httpRequest.write("GET /" + page + " HTTP/" + httpVersion + CRLF);
            httpRequest.write("Host:" + host + "" + CRLF);
            httpRequest.write("User-Agent: Mozilla/5.0" + CRLF);
            httpRequest.write("Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8" + CRLF);
            httpRequest.write(CRLF);
            // Sending HTTP request
            httpRequest.flush();
        }
        catch (Exception sendHttpRequestException)
        {
            sendHttpRequestException.printStackTrace();
        }
    }

    public static void StartClient(String host, String page, int port, boolean download, String httpVersion)
    {
        try
        {
            // Creating TCP client socket to connecting to the web server
            InetAddress address = InetAddress.getByName(host);
            Socket clientSocket = new Socket(address, port);
            // Sending HTTP request
            BufferedWriter httpRequest = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream(), StandardCharsets.UTF_8));
            SendHttpRequest(httpRequest, clientSocket, host, page, httpVersion);
            // Creating input stream
            BufferedReader inputStream = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            // Downloading statement
            if (download)
            {
                DownloadFile(inputStream, page);
            }
            // Displaying statement
            else
            {
                // Getting and printing HTTP response
                String httpResponse;
                while ( (httpResponse = inputStream.readLine()) != null )
                {
                    System.out.println(httpResponse);
                }
            }
            // Closing buffer
            httpRequest.close();
            inputStream.close();
        }
        catch (Exception clientException)
        {
            clientException.printStackTrace();
        }
    }

    public static void ShowHelp()
    {
        System.out.println("HTTP Client Socket usage :\n" +
                "For show help, use '-h'.\n" +
                "\n" +
                "java HttpClientSocket.java {-h}");
    }

    public static void main(String[] args)
    {
        // Default settings - Please change for your purpose //
        String host = "localhost";
        String page = "";
        int port = 80;
        boolean download = false;
        String httpVersion = "1.0";
        ///////////////////////////////////////////////////////

        boolean executable = true;
        // Getting commande line arguments
        try
        {
            // Command line arguments statement
            if (args.length != 0)
            {
                // Show help statement
                if (args[0].equals("-h"))
                {
                    ShowHelp();
                }
                // Personalized configuration statement
                else if ( args.length == 10 && args[0].equals("--host") && args[2].equals("--page") && args[4].equals("--port") && args[6].equals("--download") && args[8].equals("--httpversion") )
                {
                    host = args[1];
                    page = args[3];
                    try
                    {
                        port = Integer.parseInt(args[5]);
                    }
                    catch (Exception exceptionPortArgs)
                    {
                        exceptionPortArgs.printStackTrace();
                        System.out.println("Error in port argument value : please enter a valid integer !");
                        executable = false;
                    }
                    if (args[7].equals("false") ^ args[7].equals("true"))
                    {
                        download = Boolean.parseBoolean(args[7]);
                    }
                    else
                    {
                        System.out.println("Error in download argument value : please enter a valid boolean !");
                        executable = false;
                    }
                    httpVersion = args[9];
                }
                // Parameters error statement
                else
                {
                    System.out.println("Errors in parameters !\n");
                    ShowHelp();
                }
            }
            // Execution statement
            if (executable)
            {
                // Starting HTTP client
                StartClient(host, page, port, download, httpVersion);
            }
        }
        catch (Exception mainException)
        {
            mainException.printStackTrace();
        }
    }
}