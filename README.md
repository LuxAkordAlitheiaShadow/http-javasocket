# HTTP-JavaSocket

Deux scripts d'exemple d'utilisation des `Sockets Java` dans le cadre d'une utilisation du protocole `HTTP`.

Lien du dépot Git : https://gitlab.com/LuxAkordAlitheiaShadow/http-javasocket

## Client HTTP

Le but du programme `HttpClientSocket.java` est de simuler un client HTTP, comme pourrait réagir un navigateur.
Le programme est capable d'envoyer une requête HTTP de type `GET` sur un port donné.
Le client étant une application CLI, il peut demander, par défaut, de recevoir la réponse HTTP du serveur sur la sortie standard ou de télécharger la ressources demandée.

### Utilisation 

Pour lancer le programme avec les valeurs par défaut :

`java HttpClientSocket.java`

Pour lancer le programme avec des paramètres spécifiques :

`java HttpClientSocket.java --host [ip_or_domain_name] --page [ressource] --port [port_value] --download [boolean] --httpversion [http_version]`

**Il est important de bien renseigner tous les paramètres pour une utilisation avec un paramètre spécifique**

Pour afficher l'aide :

`java HttpClientSocket.java -h`

Pour lancer le programme avec des paramètres spécifiques, veuillez changer les valeurs, dans le fichier sources, lignes 138-144.

### Exemple d'utilisation

Exemple d'utilisation du programme client avec les paramètres dans le code source suivant :

- host : "example.com"
- page : "" (défaut)
- port : 80 (défaut)
- download : false (défaut)
- httpVersion : "1.0" (défaut)

![Client_200](./Ressources/Client_200.png)

Exemple d'utilisation avec les mêmes paramètres, mais en ligne de commande uniquement :

![Client_200_CLI](./Ressources/Client_200_CLI.png)

Exemple d'utilisation avec les mêmes paramètres, mais en mode téléchargeable :

- host : "example.com"
- page : "" (défaut)
- port : 80 (défaut)
- download : true
- httpVersion : "1.0" (défaut)

![Client_download](./Ressources/Client_download.png)
![File_downloaded](./Ressources/File_downloaded.png)

Si le client télécharge une page avec le même nom qu'une autre page présente dans le repertoire locale :

![Client_download_again](./Ressources/Client_download_again.png)
![File_downloaded_again](./Ressources/File_downloaded_again.png)

## Serveur HTTP

Le but du programme `HttpServerSocket.java` est de simuler un serveur HTTP.
Il est capable de recevoir des requetes HTTP de type `GET` et de renvoyer la ressource demandée avec un code de succès HTTP 200.
Dans le cas ou la ressource est inexistante, un code d'erreur client HTTP 404 avec la page d'erreur est renvoyé.

### Utilisation

Pour lancer le programme avec les valeurs par défaut :

`java HttpServerSocket.java`

Pour lancer le programme avec une valeur de port spécifique :

`java HttpServerSocket.java -p [port_value]`

Pour afficher l'aide :

`java HttpServerSocket.java -h`

### Exemple d'utilisation

Exemple d'utilisation avec un port spécifique (8888) :

![run_server_with_port](./Ressources/run_server_with_port.png)

Si nous demandons une requete HTTP depuis un navigateur, dans notre cas `http://localhost:8888/`, nous obtenons la ressource demandée :

![get_server_200](./Ressources/get_server_200.png)

Et le serveur journalise la requete avec code de succès 200 :

![Server_logs_200](./Ressources/Server_logs_200.png)

Dans le cas ou l'on demande une ressource inexistante pour le serveur, dans notre cas `http://localhost:8888/unexisting_file` :

![Get_server_404](./Ressources/Get_server_404.png)

Et le serveur journalise la requete avec code d'erreur 404 :

![Server_logs_404](./Ressources/Server_logs_404.png)
