import java.net.*;
import java.io.*;
import java.util.Date;
import java.util.Scanner;

/**
 * @Package: HttpServerSocket
 * @Author: Titouan 'Lux' Allain
 * @Version: 1.0
 */

public class HttpServerSocket
{
    public static String AdjustRequestedRessource(String requestedRessource)
    {
        requestedRessource = requestedRessource.substring(1, requestedRessource.length()); // Removing first `/`
        // Asking default page statement
        if (requestedRessource == "")
        {
            requestedRessource = "index.html";
        }
        return requestedRessource;
    }

    public static String CreateHttpResponse(String requestedRessource)
    {
        String CRLF = "\r\n";
        String httpResponse = "";

        // Fetching requested file
        Date date = new Date();
        requestedRessource = AdjustRequestedRessource(requestedRessource);
        System.out.println(date + " - Fetching " + requestedRessource + " file.");

        File requestedFile = new File("./html/" + requestedRessource);
        Scanner fileContent = null;
        // File exists statement
        if (requestedFile.exists())
        {
            System.out.println("Founded");
            httpResponse = httpResponse.concat("HTTP/1.1 200 OK" + CRLF);
        }
        // File doesn't exists statement
        else
        {
            System.out.println("File is missing");
            httpResponse = httpResponse.concat("HTTP/1.1 404 Not Found" + CRLF);
            // Reading 404 error file
            requestedFile = new File("./html/404_error.html");
        }
        // Reading requested file
        try
        {
            fileContent = new Scanner(requestedFile);
        }
        catch (Exception fileException)
        {
            fileException.printStackTrace();;
        }
        // Forging HTTP server headers
        date = new Date();
        httpResponse = httpResponse.concat("date: " + date + CRLF);
        httpResponse = httpResponse.concat("server: JavaSocketLux/1.0" + CRLF);
        httpResponse = httpResponse.concat("content-type: text/html" + CRLF);
        httpResponse = httpResponse.concat(CRLF);
        // Copying file inte HTTP response
        while (fileContent.hasNextLine())
        {
            String fileLine = fileContent.nextLine();
            httpResponse = httpResponse.concat(fileLine);
        }
        return httpResponse;
    }

    public static void StartServer(int port)
    {
        try
        {
            // Creating web HTTP server socket
            ServerSocket serverSocket = new ServerSocket(port);
            System.out.println("Server started on port " + port + ".");
            while (true)
            {
                // Waiting for client connexion
                Socket clientSocket = serverSocket.accept();
                Date date = new Date();
                System.out.println(date + " - New connexion established.");

                // Reading HTTP client request
                InputStreamReader inputStreamFromClient =  new InputStreamReader(clientSocket.getInputStream());
                BufferedReader dataFromClient = new BufferedReader(inputStreamFromClient);
                String line = dataFromClient.readLine();
                String[] httpRequest = line.split(" ");

                // Creating HTTP response
                String httpResponse = CreateHttpResponse(httpRequest[1]);

                // Sending HTTP response to the client
                System.out.println(date + " - Responding : \r\n" + httpResponse);
                clientSocket.getOutputStream().write(httpResponse.getBytes("UTF-8"));
                // Closing connexion
                date = new Date();
                System.out.println(date + " - Closing connexion");
                dataFromClient.close();
                clientSocket.close();
            }
        }
        catch (Exception serverException)
        {
            serverException.printStackTrace();
        }
    }

    public static void ShowHelp()
    {
        System.out.println("HTTP Server Socket usage :\n" +
                "For show help, use '-h'.\n" +
                "For setting port, use '-p [port_value]', the default value is 80.\n" +
                "\n" +
                "java HttpServerSocket.java {-h | -p [port_value]}");
    }

    public static void main(String[] args)
    {
        // Default settings - Please change for your purpose //
        int port = 80;
        ///////////////////////////////////////////////////////

        try
        {
            // Command line arguments statement
            if (args.length != 0)
            {
                // Show help statement
                if (args[0].equals("-h"))
                {
                    ShowHelp();
                }
                // Set port statement
                else if (args[0].equals("-p") && !args[1].equals(""))
                {
                    try
                    {
                        port = Integer.parseInt(args[1]);
                        // Starting HTTP server with new port
                        StartServer(port);
                    }
                    catch (Exception pArgsException)
                    {
                        pArgsException.printStackTrace();
                        System.out.println("Error : Please use an integer for the port_value parameter.");
                    }
                }
                // Parameters error statement
                else
                {
                    System.out.println("Errors in parameters !\n");
                    ShowHelp();
                }
            }
            // Default execution statement
            else
            {
                // Starting HTTP server with default port
                StartServer(port);
            }
        }
        catch (Exception mainException)
        {
           mainException.printStackTrace();;
        }
    }
}